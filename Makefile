CA=ca65
LD=ld65
CAFLAGS=--cfg-path nes.cfg
SOURCES=src/blink.s
OBJECTS=$(SOURCES:.s=.o)

LINK=$(LD) -o $@.nes -t nes src/$< $(CAFLAGS)
COMPILE=$(CA) $<

all: blink

blink: blink.o
	$(LINK)
	
blink.o: src/blink.s
	$(COMPILE)
	
clean:
	rm -f *.o *.nes
